#ifndef CAMDEVICE_H
#define CAMDEVICE_H

#include <QFile>
#include <QIODevice>

class CamDevice : public QIODevice
{
	Q_OBJECT

	QFile	*file;
	QByteArray	arr;
	int	possition = 0;

public:
	CamDevice(QObject *parent);

	// QIODevice interface
protected:
	qint64 readData(char *data, qint64 maxlen) override;

	// QIODevice interface
public:
	bool isSequential() const override;
	bool open(OpenMode mode) override;
	//void close() override;
	qint64 pos() const override;
	qint64 size() const override;
	bool seek(qint64 pos) override;
	bool atEnd() const override;
	bool reset() override;
	qint64 bytesAvailable() const override;
	bool waitForReadyRead(int msecs) override;

protected:
	qint64 writeData(const char *data, qint64 len) override {};

private slots:
	void readFrom();
};

#endif // CAMDEVICE_H

#include "mainwindow.h"

#include <QApplication>
#include <QCoreApplication>
#include <QTime>
//#include <vlc.hpp>
#include <thread>
#include <iostream>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	Q_UNUSED(type)

	QString str = QString::asprintf("[%s][%s, %d] %s", QTime::currentTime().toString().toLocal8Bit().constData(),
									context.function, context.line, msg.toLocal8Bit().constData());
	std::cout << str.toStdString() << std::endl;

}

int main(int argc, char *argv[])
{
	qInstallMessageHandler(myMessageOutput);
	QApplication a(argc, argv);
	/*QCoreApplication a(argc, argv);

	if (argc < 2)
	{
		std::cerr << "usage: " << argv[0] << " <file to play>" << std::endl;
		return 1;
	}

	auto instance = VLC::Instance(0, nullptr);
	auto media = VLC::Media(instance, argv[1], VLC::Media::FromPath);
	auto mp = VLC::MediaPlayer(media);
	mp.play();
	std::this_thread::sleep_for( std::chrono::seconds( 10 ) );

	mp.stop();*/


	MainWindow w;
	w.show();
	return a.exec();
}

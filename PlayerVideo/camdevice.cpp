#include "camdevice.h"

#include <QDebug>
#include <QTimer>

CamDevice::CamDevice(QObject *parent) : QIODevice(parent)
{
	file = new QFile("F:/Downloads/video_04_03_2019__16_35_33__0001.ts");
	file->open(QFile::ReadOnly);

	QTimer	*t = new QTimer(this);
	connect(t, &QTimer::timeout, this, &CamDevice::readFrom);
	t->start(100);
	readFrom();
}

qint64 CamDevice::readData(char *data, qint64 maxlen)
{
	qDebug() << maxlen;
	char *p = data;
	while ((possition < arr.size()) && maxlen--) {
		*p++ = arr[possition++];
	}
	return p-data;

	//return file->read(data, maxlen);
}

bool CamDevice::isSequential() const
{
	qDebug();
	//return QIODevice::isSequential();
	return false;
}

bool CamDevice::open(QIODevice::OpenMode mode)
{
	qDebug() << mode;
	return QIODevice::open(mode);
}

qint64 CamDevice::pos() const
{
	auto	v = possition;
	qDebug() << v;
	//return QIODevice::pos();
	return v;
}

qint64 CamDevice::size() const
{
	auto	v = arr.size();
	qDebug() << v;
	//return QIODevice::size();
	return v;
}

bool CamDevice::seek(qint64 pos)
{
	qDebug() << pos;
	if (pos > arr.size()) return false;
	possition = pos;
	return true;
}

bool CamDevice::atEnd() const
{
	qDebug();
	return possition == arr.size()-1;
}

bool CamDevice::reset()
{
	qDebug();
	possition = 0;
	return true;
}

qint64 CamDevice::bytesAvailable() const
{
	auto	v = arr.size();// + QIODevice::bytesAvailable();
	qDebug() << arr.size() << QIODevice::bytesAvailable();
	//return QIODevice::bytesAvailable();
	return v;
}

bool CamDevice::waitForReadyRead(int msecs)
{
	qDebug() << msecs;
	return QIODevice::waitForReadyRead(msecs);
}

void CamDevice::readFrom()
{
	arr.append(file->read(30000));
	emit readyRead();
}

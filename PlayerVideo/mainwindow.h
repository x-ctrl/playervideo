#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "camdevice.h"

#include <QMainWindow>

#include <QMediaPlayer>
#include <QVideoWidget>
#include <QBuffer>
#include <QFile>
#include <QUdpSocket>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

	QMediaPlayer	*player;
	QVideoWidget	*vwidget;

	QBuffer	*buff;
	QFile	*file;
	CamDevice	*cdev;
	QUdpSocket	*udpSocket;

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	Ui::MainWindow *ui;

private slots:
	void MediaError(QMediaPlayer::Error error);
	void stateChanged(QMediaPlayer::State newState);
	void mediaStatusChanged(QMediaPlayer::MediaStatus status);
};
#endif // MAINWINDOW_H

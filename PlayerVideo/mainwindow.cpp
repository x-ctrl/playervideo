#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLayout>


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	//udpSocket = new QUdpSocket(this);
	//qDebug() << udpSocket->isSequential();

	vwidget = new QVideoWidget(this);
	setCentralWidget(vwidget);

	player = new QMediaPlayer(this, QMediaPlayer::StreamPlayback);
	player->setVideoOutput(vwidget);
	//connect(player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(MediaError(QMediaPlayer::Error)));
	connect(player, &QMediaPlayer::stateChanged, this, &MainWindow::stateChanged);
	connect(player, &QMediaPlayer::mediaStatusChanged, this, &MainWindow::mediaStatusChanged);

	//player->setMedia(QUrl::fromLocalFile("F:/Downloads/video_04_03_2019__16_35_33__0001.ts"));
	/*file = new QFile("F:/Downloads/video_04_03_2019__16_35_33__0001.ts");
	file->open(QFile::ReadOnly);

	buff = new QBuffer(this);
	buff->setData(file->readAll());
	buff->open(QFile::ReadWrite);*/

	cdev = new CamDevice(this);
	cdev->open(QFile::ReadOnly);

	player->setMedia(QMediaContent(), cdev);

	player->play();
	qDebug() << player->errorString();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::MediaError(QMediaPlayer::Error error)
{
	qDebug() <<error << player->errorString();
}

void MainWindow::stateChanged(QMediaPlayer::State newState)
{
	qDebug() << newState;
}

void MainWindow::mediaStatusChanged(QMediaPlayer::MediaStatus status)
{
	qDebug() << status;
}
